Feature: login to Gmail and send message

  Scenario Outline: check if I can send a message to an invalid email and a valid email
    Given I am on Gmail login page
    When I am logged in as <email> with <password>
    And I send a message to an invalid email
      | invalidEmail | subject  | message         |
      | 347bVbsds    | Selenium | I like Selenium |
    Then I see alert message is appeared
      | invalidEmail |
      | 347bVbsds    |
    When I close alert
    And I send a message to a valid email
      | receiver                 | subject  | message         |
      | voronovskatest@gmail.com | Selenium | I like Selenium |
    Then I see that message sent successfully
      | message         |
      | I like Selenium |
    And I driver is closed

    Examples:
      | email                     | password          |
      | vira7petrova786@gmail.com | petrova786        |
      | voronovska7890@gmail.com  | minolatepat7890V1 |
