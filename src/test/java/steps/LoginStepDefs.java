package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import driver.DriverManager;
import po.GmailLoginPO;
import util.PropertiesReader;

public class LoginStepDefs {
    GmailLoginPO gmailLoginPO;

    public LoginStepDefs() {
        this.gmailLoginPO = new GmailLoginPO(DriverManager.getDriver());
    }

    @Given("I am on Gmail login page")
    public void openGmailLoginPage() {
        DriverManager.getDriver().get(PropertiesReader.getInstance().getGmailURL());
    }

    @When("^I am logged in as ([^\"]*) with ([^\"]*)$")
    public void loginUser(String login, String password) {
        gmailLoginPO.inputLoginAndClick(login);
        gmailLoginPO.inputPasswordAndClick(password);
    }
}
