package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.DriverManager;
import model.Message;
import org.testng.Assert;
import po.AlertPO;
import po.GmailHomePagePO;

import java.util.List;

public class HomePageStepDefs {
    private GmailHomePagePO gmailHomePagePO;
    private AlertPO alertPO;


    public HomePageStepDefs() {
        this.gmailHomePagePO = new GmailHomePagePO(DriverManager.getDriver());
        this.alertPO = new AlertPO(DriverManager.getDriver());
    }

    @And("^I send a message to an invalid email$")
    public void sendMessageWithInvalidEmail(List<Message> messages) {
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo(messages.get(0).getInvalidEmail());
        gmailHomePagePO.writeASubjectOfMessage(messages.get(0).getSubject());
        gmailHomePagePO.writeAMessage(messages.get(0).getMessage());
        gmailHomePagePO.sendAMessage();
    }

    @And("^I send a message to a valid email$")
    public void sendMessageWithValidEmail(List<Message> messages) {
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo(messages.get(0).getReceiver());
        gmailHomePagePO.writeASubjectOfMessage(messages.get(0).getSubject());
        gmailHomePagePO.writeAMessage(messages.get(0).getMessage());
        gmailHomePagePO.sendAMessage();
    }


    @Then("^I see alert message is appeared$")
    public void verifyAlertIsPresent(List<Message> message) {
        Assert.assertTrue(alertPO.getAlertMessage().contains(message.get(0).getInvalidEmail()));
    }

    @When("^I close alert$")
    public void clickOnCloseAlertAndMessage() {
        alertPO.clickOkOnAlertPopup();
        gmailHomePagePO.clickAndCloseMessage();
    }

    @Then("^I see that message sent successfully$")
    public void verifyMessageSentSuccessfully(List<Message> message) {
        gmailHomePagePO.openSentMessage();
        Assert.assertTrue(gmailHomePagePO.getMessageDetails().contains(message.get(0).getMessage()));
    }

    @And("I driver is closed")
    public void closeBrowser() {
        DriverManager.quit();
    }
}
