import io.qameta.allure.Allure;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import util.ScreenshotMaker;

public class TestListener implements ITestListener {
    private static Logger LOG = LogManager.getLogger(TestListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        Allure.addAttachment("test finished", ScreenshotMaker.takeScreenshot());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        Allure.addAttachment("test finished successfully", ScreenshotMaker.takeScreenshot());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        LOG.info("Test Failure!");
        Allure.addAttachment("test failed", ScreenshotMaker.takeScreenshot());
    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {
    }
}
