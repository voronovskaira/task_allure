package decorator;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Button extends Element {
    public Button(WebElement webElement) {
        super(webElement);
    }

    public void click(){
        new Actions(DriverManager.getDriver()).clickAndHold(webElement).release().perform();
    }
}
