package util;

import com.opencsv.CSVReader;
import model.User;
import model.Users;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class OpenCSVParser {

    public static Users getUsersFromCSV(String filePath) {

        CSVReader reader = null;
        Users users = new Users();
        try {
            reader = new CSVReader(new FileReader(new File(filePath)));
            String[] line;
            while ((line = reader.readNext()) != null) {
                users.addUserToList(new User(line[0], line[1]));
            }
            return users;
        } catch (IOException e) {
            System.out.println(String.format("problem with csv file %s", filePath));
        }
        return null;
    }
}


