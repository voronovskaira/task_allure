package util;

import driver.DriverManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ScreenshotMaker {

    public static InputStream takeScreenshot() {
        InputStream screenshot = null;
        try {
            screenshot = new FileInputStream(((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.FILE));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return screenshot;
    }
}
