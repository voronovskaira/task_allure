package po;

import decorator.Button;
import decorator.InputBox;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class GmailLoginPO extends BasePage {
    @FindBy(xpath = "//*[@id='identifierId']")
    private InputBox login;

    @FindBy(xpath = "//*[@id='identifierNext']/span/span")
    private Button submitLogin;

    @FindBy(name = "password")
    private InputBox password;

    @FindBy(id = "passwordNext")
    private Button submitPassword;

    public GmailLoginPO(WebDriver driver) {
        super(driver);
    }

    public void inputLoginAndClick(String login) {
        this.login.sendKeys(login);
        this.submitLogin.click();
    }

    public void inputPasswordAndClick(String password) {
        this.password.sendKeys(password);
        new Actions(driver).clickAndHold(submitPassword.getWebElement()).release().perform();
    }
}
