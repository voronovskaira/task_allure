package bo;

import driver.DriverManager;
import io.qameta.allure.Step;
import po.GmailLoginPO;

public class LoginBO {
    GmailLoginPO gmailLoginPO;

    public LoginBO() {
        this.gmailLoginPO = new GmailLoginPO(DriverManager.getDriver());
    }

    @Step("User login to gmail")
    public void loginUser(String login, String password) {
        gmailLoginPO.inputLoginAndClick(login);
        gmailLoginPO.inputPasswordAndClick(password);
    }
}
